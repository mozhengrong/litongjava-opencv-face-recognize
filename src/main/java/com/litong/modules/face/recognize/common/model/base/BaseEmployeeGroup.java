package com.litong.modules.face.recognize.common.model.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Generated by JFinal, do not modify this file.
 */
@SuppressWarnings("serial")
public abstract class BaseEmployeeGroup<M extends BaseEmployeeGroup<M>> extends Model<M> implements IBean {

	/**
	 * 主键
	 */
	public void setId(java.lang.Long id) {
		set("id", id);
	}
	
	/**
	 * 主键
	 */
	public java.lang.Long getId() {
		return getLong("id");
	}
	
	/**
	 * 分类名称
	 */
	public void setName(java.lang.String name) {
		set("name", name);
	}
	
	/**
	 * 分类名称
	 */
	public java.lang.String getName() {
		return getStr("name");
	}
	
	/**
	 * 分类描述
	 */
	public void setDesc(java.lang.String desc) {
		set("desc", desc);
	}
	
	/**
	 * 分类描述
	 */
	public java.lang.String getDesc() {
		return getStr("desc");
	}
	
	/**
	 * 软删除
	 */
	public void setDeleteTime(java.util.Date deleteTime) {
		set("delete_time", deleteTime);
	}
	
	/**
	 * 软删除
	 */
	public java.util.Date getDeleteTime() {
		return get("delete_time");
	}
	
	/**
	 * 创建时间
	 */
	public void setCreateTime(java.util.Date createTime) {
		set("create_time", createTime);
	}
	
	/**
	 * 创建时间
	 */
	public java.util.Date getCreateTime() {
		return get("create_time");
	}
	
	/**
	 * 更新时间
	 */
	public void setUpdateTime(java.util.Date updateTime) {
		set("update_time", updateTime);
	}
	
	/**
	 * 更新时间
	 */
	public java.util.Date getUpdateTime() {
		return get("update_time");
	}
	
	/**
	 * 是否删除,1删除,0未删除
	 */
	public void setIsDel(java.lang.String isDel) {
		set("is_del", isDel);
	}
	
	/**
	 * 是否删除,1删除,0未删除
	 */
	public java.lang.String getIsDel() {
		return getStr("is_del");
	}
	
}
