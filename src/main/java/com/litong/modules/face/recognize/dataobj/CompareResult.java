package com.litong.modules.face.recognize.dataobj;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author litong
 * @date 2020年9月29日_下午3:01:21 
 * @version 1.0 
 * @desc
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CompareResult {
  private boolean isSame;
  private Double coefficient;
  
}
