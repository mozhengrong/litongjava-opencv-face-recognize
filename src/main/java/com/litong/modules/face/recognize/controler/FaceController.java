package com.litong.modules.face.recognize.controler;

import java.io.File;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.Kv;
import com.jfinal.upload.UploadFile;
import com.litong.jfinal.render.MimeTypeRender;
import com.litong.jfinal.vo.JsonBean;
import com.litong.modules.face.recognize.dataobj.DetectResult;
import com.litong.modules.face.recognize.service.FaceService;
import com.litong.modules.face.recognize.utils.CascadeClassifierUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bill robot
 * @date 2020年9月6日_下午4:15:48 
 * @version 1.0 
 * @desc
 */
@Slf4j
public class FaceController extends Controller {

  @Inject
  FaceService faceService;

  /**
   * 返回文件路径,判断文件是否存在
   */
  public void xmlPath() {
    String frontalfaceAltXml = CascadeClassifierUtils.getFrontalfaceAltXml();
    String eyeXml = CascadeClassifierUtils.getEyeXml();
    Kv kv = Kv.create();
    kv.set("frontalfaceAltXml", frontalfaceAltXml);
    kv.set("frontalfaceAltXmlExists", new File(frontalfaceAltXml).exists());
    kv.set("eyeXml", eyeXml);
    kv.set("eyeXmlExists", new File(eyeXml).exists());
    JsonBean<Kv> jsonBean = new JsonBean<>(kv);
    renderJson(jsonBean);
  }

  /**
   * 检测图片是否包含人脸
   */
  public void detect() {
    UploadFile uploadFile = getFile();
    DetectResult detectResult = faceService.detect(uploadFile);
    JsonBean<DetectResult> jsonBean = new JsonBean<DetectResult>(detectResult);
    renderJson(jsonBean);
  }

  /**
   * 上传人人脸图片
   */
  public void uploadImageFace() {
    renderJson(faceService.uploadImageFace(getFile()));
  }

  /**
   * 人脸搜索
   */
  @SuppressWarnings("unchecked")
  public void search() {
    File file = getFile().getFile();
    String employeeDepartmentId = getPara("employeeDepartmentId");
    String employeeGroupId = getPara("employeeGroupId");
    String employeeTypeId = getPara("employeeTypeId");
    log.info("{},{},{}", employeeDepartmentId, employeeGroupId, employeeTypeId);
    Kv kv = Kv.create();
    kv.put("department_id", employeeDepartmentId);
    kv.put("group_id", employeeGroupId);
    kv.put("type_id", employeeTypeId);
    renderJson(faceService.search(file,kv));
    return;
  }

  /**
   * 返回人脸图片
   */
  public void image(Kv kv) {
    String id = kv.getStr("id");
    String uri = faceService.getUriByImageId(id);
    MimeTypeRender mimeTypeRender = new MimeTypeRender("jpg", uri);
    render(mimeTypeRender);
  }

  /**
   * 测试图片上传
   */
  public void upload() {
    UploadFile uploadFile = getFile();
    String uploadPath = uploadFile.getUploadPath();
    String fileName = uploadFile.getFileName();
    String contentType = uploadFile.getContentType();
    String originalFileName = uploadFile.getOriginalFileName();
    String parameterName = uploadFile.getParameterName();
    Kv kv = Kv.create();
    kv.set("uploadPath", uploadPath);
    kv.set("fileName", fileName);
    kv.set("contentType", contentType);
    kv.set("originalFileName", originalFileName);
    kv.set("parameterName", parameterName);
//    uploadFile.getFile().delete();
    renderJson(kv);
  }
}
